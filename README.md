[![Go Report Card](https://goreportcard.com/badge/github.com/Stalis/api-service)](https://goreportcard.com/report/github.com/Stalis/api-service)

# api-service
This is my project template for learning microservices on golang. Now in active development and architecture may critically changed, but in future, it will be stable project layout template.

# Docker
## Environment variables
- `DEBUG` - `bool (default: false)` Use debug logging
- `LOG_LEVEL` - `string (default: "debug")` Using zerolog log levels
- `SERVER_HOST` - `string (default: "")` Host for HTTP matching
- `SERVER_PORT` - `int (default: "80")` Port for HTTP listener
- `SERVER_WRITE_TIMEOUT` - `time.Duration (default:"15s")`
- `SERVER_READ_TIMEOUT` - `time.Duration (default:"15s")`
- `SERVER_IDLE_TIMEOUT` - `time.Duration (default:"60s")`
- `SERVER_SHUTDOWN_WAIT` - `time.Duration (default:"15s")` Timeout for graceful shutdown
- `DATABASE_URL` - `string` URI for Postgres database
