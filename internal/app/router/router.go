package router

import (
	"encoding/json"
	"net/http"

	"api-service/internal/app"
	"api-service/internal/handlers/account"
	"api-service/internal/handlers/session"
	"api-service/internal/middlewares"

	"github.com/gorilla/mux"
)

func New(a *app.App) http.Handler {
	router := mux.NewRouter()
	router.StrictSlash(true)

	router.Use(middlewares.NewRecovery(a.Logger).Middleware)
	router.Use(middlewares.NewRequestLogger(a.Logger).Middleware)
	if a.Config.Common.IsDebug {
		router.Use(middlewares.NewRequestBodyLogger(a.Logger).Middleware)
	}

	apiRouter := router.PathPrefix("/api").Subrouter()
	RouteApi(apiRouter, a)

	return router
}

func RouteApi(r *mux.Router, a *app.App) {
	r.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		// an example API handler
		json.NewEncoder(w).Encode(map[string]bool{"ok": true})
	})

	accountRouter := r.PathPrefix("/account").Subrouter()
	accountRouter.Handle("/login", account.NewLogin(a.Logger, a.DbContext, a.DbContext)).
		Methods(http.MethodPost)
	accountRouter.Handle("/register", account.NewRegister(a.Logger, a.DbContext)).
		Methods(http.MethodPost)

	sessionRouter := r.PathPrefix("/session").Subrouter()
	sessionRouter.Handle("/check", session.NewCheck(a.Logger, a.DbContext)).
		Methods(http.MethodPost)

	securedRouter := r.PathPrefix("/").Subrouter()
	securedRouter.Use(middlewares.NewAuthentication(a.Logger, a.DbContext).Middleware)
	securedRouter.
		Handle("/hello", http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			rw.Write([]byte("hello"))
		})).
		Methods(http.MethodGet)
}
