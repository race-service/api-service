package app

import (
	"api-service/internal/config"
	"api-service/internal/dal"
	"api-service/internal/util/logger"
)

type App struct {
	Logger    *logger.Logger
	Config    *config.Config
	DbContext *dal.DB
}

func New(
	log *logger.Logger,
	conf *config.Config,
	db *dal.DB) *App {
	return &App{log, conf, db}
}
