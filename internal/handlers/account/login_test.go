package account_test

import (
	"api-service/internal/dal"
	"api-service/internal/handlers/account"
	"api-service/internal/util/logger"
	"bytes"
	"encoding/json"
	"io"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
)

func mockLogger() *logger.Logger {
	return logger.NewWithWriter(io.Discard)
}

func getFakeUsers() []*dal.User {
	return append(
		[]*dal.User{},
		&dal.User{
			Username:     "Alice",
			PasswordHash: "123",
			CreatedDate:  time.Now(),
		},
		&dal.User{
			Username:     "Bob",
			PasswordHash: "asd",
			CreatedDate:  time.Now(),
		},
		&dal.User{
			Username:     "Charlie",
			PasswordHash: "!@#",
			CreatedDate:  time.Now(),
		},
	)
}

type mockUserRepository struct {
	users []*dal.User
}

func (r *mockUserRepository) GetUserByUsername(username string) (*dal.User, error) {
	for _, item := range r.users {
		if item.Username == username {
			return item, nil
		}
	}
	return nil, errors.New("Not found User")
}

type mockSessionCreator struct{}

func (r *mockSessionCreator) CreateSession(userId int64) (uuid.UUID, error) {
	return uuid.Nil, nil
}

func Test_login_ServeHTTP(t *testing.T) {
	type fields struct {
		log      *logger.Logger
		users    account.UserByUsernameGetter
		sessions account.SessionCreator
	}
	type args struct {
		json string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr string
	}{
		{
			name: "Valid input",
			fields: fields{
				log:      mockLogger(),
				users:    &mockUserRepository{users: getFakeUsers()},
				sessions: &mockSessionCreator{},
			},
			args: args{
				json: "{ \"username\": \"Alice\", \"password\": \"123\" }",
			},
		},
		{
			name: "Invalid password",
			fields: fields{
				log:      mockLogger(),
				users:    &mockUserRepository{users: getFakeUsers()},
				sessions: &mockSessionCreator{},
			},
			args: args{
				json: "{ \"username\": \"Alice\", \"password\": \"1234\" }",
			},
			wantErr: "incorrect password\n",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rw := httptest.NewRecorder()
			r := httptest.NewRequest(
				"POST", "http://example.com/api/account",
				strings.NewReader(tt.args.json),
			)

			h := account.NewLogin(
				tt.fields.log,
				tt.fields.users,
				tt.fields.sessions,
			)

			h.ServeHTTP(rw, r)
			defer rw.Result().Body.Close()

			payload, err := io.ReadAll(rw.Result().Body)
			if err != nil {
				t.Error(err)
				t.FailNow()
			}

			if tt.wantErr != "" {
				str := string(payload)
				if str == tt.wantErr {
					return
				}
				t.Errorf("Login.ServeHTTP(rw, r) error = %v, wantErr %v", str, tt.wantErr)
				t.FailNow()
			}

			t.Log(string(payload))
			response := &account.LoginResponse{}
			err = json.Unmarshal(payload, response)
			if err != nil {
				t.Error(err)
				t.FailNow()
			}

			if response.SessionToken != uuid.Nil {
				t.FailNow()
			}
		})
	}
}

func TestLoginRequest_FromJSON(t *testing.T) {
	type args struct {
		input string
	}
	type result struct {
		Username     string
		PasswordHash string
	}
	tests := []struct {
		name    string
		args    args
		wantRes result
		wantErr bool
	}{
		{
			name:    "Valid JSON",
			args:    args{input: `{ "username": "User", "password": "1234" }`},
			wantRes: result{"User", "1234"},
		},
		{
			name:    "Invalid JSON",
			args:    args{input: `{ "" }`},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := &account.LoginRequest{}

			if err := l.FromJSON(strings.NewReader(tt.args.input)); (err != nil) != tt.wantErr {
				t.Errorf("LoginRequest.FromJSON() error = %v, wantErr %v", err, tt.wantErr)
				t.FailNow()
			}

			if l.Username != tt.wantRes.Username {
				t.Errorf("LoginRequest.FromJSON().Username = %v, wantR %v", l.Username, tt.wantRes.Username)
			}

			if l.PasswordHash != tt.wantRes.PasswordHash {
				t.Errorf("LoginRequest.FromJSON().PasswordHash = %v, wantR %v", l.PasswordHash, tt.wantRes.PasswordHash)
			}
		})
	}
}

func TestLoginResponse_ToJSON(t *testing.T) {
	type fields struct {
		SessionToken uuid.UUID
	}
	tests := []struct {
		name    string
		fields  fields
		wantW   string
		wantErr bool
	}{
		{
			name:    "Valid JSON",
			fields:  fields{uuid.Nil},
			wantW:   `{"sessionToken":"` + uuid.Nil.String() + "\"}\n",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := &account.LoginResponse{
				SessionToken: tt.fields.SessionToken,
			}
			w := &bytes.Buffer{}
			if err := l.ToJSON(w); (err != nil) != tt.wantErr {
				t.Errorf("LoginResponse.ToJSON() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotW := w.String(); gotW != tt.wantW {
				t.Errorf("LoginResponse.ToJSON() = %v, want %v", gotW, tt.wantW)
			}
		})
	}
}
